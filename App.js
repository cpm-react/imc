import React, {Component} from 'react';
import {SafeAreaView, Text, StyleSheet, TextInput} from 'react-native';
import TextInputMask from 'react-native-text-input-mask';


function validaImc(imc){
  if (isNaN(imc) || imc > 500 || imc < 10) {
    return '';
  }

  var intepretacao = 'OBESIDADE GRAVE'
  if (imc < 40) {
    intepretacao = 'OBESIDADE'
  }
  if (imc < 30) {
    intepretacao = 'SOBREPESO'
  }
  if (imc < 25) {
    intepretacao = 'NORMAL'
  }
  if (imc < 18.5) {
    intepretacao = 'MAGREZA'
  }

  return imc.toFixed(2) + " - " + intepretacao;
}
class App extends Component {

  constructor(props){
    super(props)

    this.state = {
      altura: '',
      peso: '',
      imc: '',
    }
  }
  handleChange = (event, name) => {  
    const {target, eventCount, text} = event.nativeEvent;
    if (name === 'altura') {
      this.state.altura = text;
    } else if (name === 'peso') {
      this.state.peso = text;
    }

    var Peso = this.state.peso ?? 0;
    var Altura = this.state.altura ?? 0;

    switch(name) {
      case 'altura':
        this.setState({altura:text});
        break;
      case 'peso':
        this.setState({peso:text});
        break;
    }

    var imc = Peso/(Altura*Altura)
    this.setState({imc:validaImc(imc)});
  };
  render() {

    return (
      <SafeAreaView style={styles.view}>
      <Text style={styles.h1}>IMC</Text>
      <Text style={styles.h2}>{this.state.imc}</Text>

        <TextInputMask
          style={styles.input}
          placeholder="Peso"
          keyboardType="decimal-pad"
          maxLength={6}
          name="peso"
          onChange={e => this.handleChange(e, 'peso')}
          mask="[999].[9]"
        />

        <TextInputMask
          style={styles.input}
          placeholder="Altura (em metros)"
          keyboardType="decimal-pad"
          maxLength={4}
          name="altura"
          onChange={e => this.handleChange(e, 'altura')}
          mask="[9].[99]"
        />
      </SafeAreaView>
    );
  }
}


const styles = StyleSheet.create({
  view: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#383a59'
  },
  h1: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#bd93f9'
  },
  h2: {
    fontSize: 20,
    color: '#bd93f9'
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    width: '80%',
    borderColor: '#bd93f9',
    color: "#bd93f9"
  },
});


export default App;